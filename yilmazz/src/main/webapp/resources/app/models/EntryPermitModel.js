
define([
    'backbone',
    'backbone-relational',
    'app/models/PlaceModel',
    'app/models/EntryCauseModel',
    'app/collections/EntryCauseCollection',
    'app/collections/PlaceCollection',
    'app/models/CompanyModel',
], function(Backbone,PlaceModel,EntryCauseModel,EntryCauseCollection,PlaceCollection,CompanyModel){



    var EntryPermitModel=Backbone.Model.extend({

        urlRoot:'/rest/entryPermit',
        validate:function (ev) {
        debugger
            if(ev.entryCauses.trim()==""){
                return "entryCausesAdd";
            }
            if(ev.entryCompany.trim()==""){
                return "entryCompanyAdd";
            }
            if(ev.entryDate.trim()==""){
                return "entryDateAdd";
            }

            if(ev.entryPersons.trim()==""){
                return "entryPersonsAdd";
            }
            if(ev.exitCompany.trim()==""){
                return "exitCompanyAdd";
            }
            if(ev.exitDate.trim()==""){
                return "exitDateAdd";
            }
            if(ev.jobSecure==null){
                return "jobSecureAdd";
            }
            if(ev.places.trim()==""){
                return "placesAdd";
            }
            if(ev.withPerson.trim()==""){
                return "withPersonAdd";
            }
            if(ev.company.id==0){
                return "companyAdd";
            }







        }






    });

    return EntryPermitModel;



});




