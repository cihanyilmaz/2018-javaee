define(['jquery', 'underscore', 'backbone','app/models/PlaceModel',], function ($, _, Backbone,PlaceModel) {



    var Places =Backbone.Collection.extend({

        url:'/rest/place',
        model:PlaceModel

    });

    return Places;


});


