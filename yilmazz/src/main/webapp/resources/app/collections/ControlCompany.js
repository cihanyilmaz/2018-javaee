define(['jquery', 'underscore', 'backbone', 'app/models/CompanyModel'], function ($, _, Backbone, CompanyModel) {


    var ControlCompany=Backbone.Collection.extend({
        model: CompanyModel,
        initialize: function ( options) {
            this.companyName = options.companyName;


        },
        url:function () {
            return "rest/company/companyName/"
                + this.companyName;

        }


    });


    return ControlCompany;





});

