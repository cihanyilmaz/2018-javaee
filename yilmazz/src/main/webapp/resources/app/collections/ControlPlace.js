define(['jquery', 'underscore', 'backbone', 'app/models/PlaceModel'], function ($, _, Backbone, PlaceModel) {


    var ControlPlace=Backbone.Collection.extend({
        model: PlaceModel,
        initialize: function ( options) {
            this.placeName = options.placeName;


        },
        url:function () {
            return "rest/place/placeName/"
                + this.placeName;

        }


    });


    return ControlPlace;





});


