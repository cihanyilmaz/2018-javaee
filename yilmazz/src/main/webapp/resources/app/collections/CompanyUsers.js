define(['jquery', 'underscore', 'backbone', 'app/models/EntryPermitModel'], function ($, _, Backbone, EntryPermitModel) {


    var IdSearch=Backbone.Collection.extend({

        model: EntryPermitModel,
        initialize: function ( options) {
            this.id = options.id;


        },
        url:function () {
            return "rest/entryControl/allCompany/"
                + this.id;

        }




    });

    return IdSearch;



});

