define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/PanelTemplate.html'

], function($, _,Backbone,Handlebars,PanelTemplate) {

    var PanelView = Backbone.View.extend({

            el: '.page',

            render: function () {

                this.$el.html(PanelTemplate);
                return this;

            }


        }
    );
    return PanelView;


});

