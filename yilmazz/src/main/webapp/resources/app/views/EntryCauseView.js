define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/EntryCauseModel',
    'text!app/templates/EntryCauseTemplate.html',
    'text!app/templates/EntryCauseListTemplate.html',
    'app/collections/ControlEntryCause',

], function($, _,Backbone,Handlebars,EntryCauseModel,EntryCauseTemplate,EntryCauseListTemplate,ControlEntryCause) {

    var EntryCauseView = Backbone.View.extend({

            el: '.page',
            events:{
                'click #saveEntryCause':'saveEntryCause',


            },
            saveEntryCause:function () {

                var entryCauseName=$("#entryCauseName").val();

                var entryCause =new EntryCauseModel();

                entryCause.set("entryCauseName",entryCauseName);
                if(!entryCause.isValid())
                {

                    $("." + entryCause.validationError).tooltip("show");
                    return false;

                }else {
                    debugger
                    var control=new ControlEntryCause({
                        entryCauseName:entryCauseName,

                    });

                    control.fetch({

                        success:function (m_entryCause) {

                            if(!m_entryCause.size()==0)
                            {

                                $('#ornekModal').modal('show');

                                return false;

                            }

                            else {

                                debugger
                                entryCause.save();
                                //window.location.href='#/newEntryCause';
                                //location.reload();
                                var entryCauseListView =new EntryCauseListView();
                                entryCauseListView.model=entryCause;
                                $("#entryCauseTemplateTable").append(entryCauseListView.render().el);

                                $("#entryCauseName").val("");
                                return false;




                            }

                        }

                    });

                    return false;
                }

                return false;


            },
            render: function () {

                this.$el.html(EntryCauseTemplate);
                return this;

            }


        }
    );

    var EntryCauseListView = Backbone.View.extend({

        tagName:'tr',
        model:EntryCauseModel,
        events: {
            'click .editEntryCause': 'editEntryCause',
            'click .updateEntryCause': 'updateEntryCause',
            'click .cancelEdit': 'cancelEdit',
            'click .deleteEntryCause': 'deleteEntryCause'

        },

        editEntryCause:function () {

            this.$el.addClass("info");//D�zenlenecek sat?ra mavi renk veriliyor.
            this.$el.find("input").show().focus();//D�zenleme modunda imle� inputa getiriliyor.
            this.$el.find("span").hide();
            $(".editEntryCause").hide();
            $(".deleteEntryCause").hide();
            this.$el.find(".updateEntryCause").show();
            this.$el.find(".cancelEdit").show();

        },

        updateEntryCause:function () {
            var entryCauseName= this.$el.find("#entryCauseName").val();

            var entryCause=new EntryCauseModel();
            entryCause.set("entryCauseName",entryCauseName);
            this.$el.removeClass("info");
            if (!entryCause.isValid()){

                $("." + entryCause.validationError).tooltip("show");


            }else{

                this.$el.removeClass("info");
                this.model.set("entryCauseName", entryCauseName);


                this.model.save();

                $(".editEntryCause").show();
                $(".deleteEntryCause").show();
                this.render();


            }


        },
        cancelEdit:function () {
            this.$el.removeClass("info");
            $(".editEntryCause").show();
            $(".deleteEntryCause").show();
            this.render();

        },
        deleteEntryCause:function () {

            var that=this;


            if(uyar())
            {
                this.$el.find(".deleteEntryCause").addClass("disabled");
                var that = this;

                this.model.destroy({
                    wait: true,success: function () {
                        that.remove();

                    },

                }); // HTTP DELETE

            }else{

                return false;
            }





        },


        render: function () {
            var that = this;

            var template = Handlebars.compile(EntryCauseListTemplate);
            var myHtml = template(that.model.toJSON());

            that.$el.html(myHtml);

            return this;

        }



    });
    return {
        EntryCauseView:EntryCauseView,
        EntryCauseListView:EntryCauseListView

    }


});


