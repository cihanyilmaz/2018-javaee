define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/EntryCauseModel',
    'app/collections/EntryCauseCollection',
    'text!app/templates/SelectEntryCauseTemplate.html',


], function($, _,Backbone,Handlebars,EntryCauseModel,EntryCauseCollection,SelectEntryCauseTemplate){

    var entryCauseCollection=new EntryCauseCollection();

    var SelectEntryCauseList=Backbone.View.extend({
        render:function (entryCauseCollection) {

            var template = Handlebars.compile(SelectEntryCauseTemplate);
            var myHtml = template({entryCauses: entryCauseCollection.toJSON()});
            $("#entryCauseList").html(myHtml);



        }



    });


    return {
        entryCauseCollection:entryCauseCollection,
        SelectEntryCauseList:SelectEntryCauseList

    }




});



