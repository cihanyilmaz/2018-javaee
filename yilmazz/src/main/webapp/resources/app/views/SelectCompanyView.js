define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/CompanyModel',
    'app/collections/CompanyCollection',
    'text!app/templates/SelectCompanyTemplate.html',


], function($, _,Backbone,Handlebars,CompanyModel,CompanyCollection,SelectCompanyTemplate){

    var companyCollection=new CompanyCollection();

    var SelectCompanyList=Backbone.View.extend({
        render:function (companyCollection) {

            var template = Handlebars.compile(SelectCompanyTemplate);
            var myHtml = template({companys: companyCollection.toJSON()});
            $("#companyList").html(myHtml);
            $("#companys").html(myHtml);
            $("#companysReport").html(myHtml);




        }



    });


    return {
        companyCollection:companyCollection,
        SelectCompanyList:SelectCompanyList

    }




});

