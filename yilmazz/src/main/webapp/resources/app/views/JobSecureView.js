define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/EntryPermitModel',
    'text!app/templates/JobSecureTemplate.html',
    'text!app/templates/JobSecureListTemplate.html',


], function($, _,Backbone,Handlebars,EntryPermitModel,JobSecureTemplate,JobSecureListTemplate) {

    var JobSecureView = Backbone.View.extend({

            el: '.page',

            render: function () {

                this.$el.html(JobSecureTemplate);
                return this;

            }


        }
    );

    var JobSecureListView = Backbone.View.extend({

        tagName:'tr',
        model:EntryPermitModel,
        events: {
            'click .jobSecure': 'jobSecure',


        },

        jobSecure:function () {

            this.model.set("jobSecure", false);


            this.model.save();

            window.location.href='#/jobSecure';
            //location.reload();

        },


        render: function () {
            var that = this;

            var template = Handlebars.compile(JobSecureListTemplate);
            var myHtml = template(that.model.toJSON());

            that.$el.html(myHtml);

            return this;

        }



    });
    return {
        JobSecureView:JobSecureView,
        JobSecureListView:JobSecureListView

    }


});



