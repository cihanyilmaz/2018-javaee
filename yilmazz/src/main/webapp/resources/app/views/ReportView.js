define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/EntryControlModel',
    'text!app/templates/ReportTemplate.html',
    'text!app/templates/ReportListTemplate.html',
    'app/collections/ReportSearchCollection'


], function($, _,Backbone,Handlebars,EntryControlModel,ReportTemplate,ReportListTemplate,ReportSearchCollection) {

    var ReportView = Backbone.View.extend({

            el: '.page',
        events: {
            'click #searchReport': 'searchReport',


        },

        searchReport:function () {

        debugger
            var id=$("#companysReport").val();
            var beginDate=$("#beginDate").val();
            var endDate=$("#endDate").val();

            var search = new ReportSearchCollection( {
                id:id,
                beginDate: beginDate,
                endDate: endDate,

            });
            search.fetch({

                success: function (m_travels) {
debugger
                    $(".resultRow").parent().empty();
                    m_travels.each(function (m_travel) {

                        var searchView = new ReportListView();
                        searchView.model = m_travel;
                        $("#ReportTemplateTable").append(searchView.render().el);
                    });




                }
            });



        },
            render: function () {

                this.$el.html(ReportTemplate);
                return this;

            }


        }
    );

    var ReportListView = Backbone.View.extend({

        tagName:'tr',
        model:EntryControlModel,



        render: function () {
            var that = this;

            var template = Handlebars.compile(ReportListTemplate);
            var myHtml = template(that.model.toJSON());

            that.$el.html(myHtml);

            return this;

        }



    });
    return {
        ReportView:ReportView,
        ReportListView:ReportListView

    }


});




