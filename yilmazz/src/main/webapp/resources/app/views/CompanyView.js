define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/CompanyModel',
    'text!app/templates/CompanyTemplate.html',
    'text!app/templates/CompanyListTemplate.html',
    'app/collections/ControlCompany',

], function($, _,Backbone,Handlebars,CompanyModel,CompanyTemplate,CompanyListTemplate,ControlCompany) {

    var CompanyView = Backbone.View.extend({

            el: '.page',
            events:{
                'click #saveCompany':'saveCompany',


            },
        saveCompany:function () {

                var companyName=$("#companyName").val();

                var company =new CompanyModel();

                company.set("companyName",companyName);
            if(!company.isValid())
            {

                $("." + company.validationError).tooltip("show");
                return false;

            }else {
                debugger
                var control=new ControlCompany({
                    companyName:companyName,

                });

                control.fetch({

                    success:function (m_company) {

                        if(!m_company.size()==0)
                        {

                            $('#warningText').text("ayni sirket tanimlayamazsin");
                            $('#ornekModal').modal('show');

                            return false;

                        }

                        else {

                            debugger
                            company.save();
                            //window.location.href='#/newCompany';
                            //location.reload();
                            var companyListView =new CompanyListView();
                            companyListView.model=company;
                            $("#companyTemplateTable").append(companyListView.render().el);

                            $("#companyName").val("");
                            return false;




                        }

                    }

                });

                return false;
            }

            return false;


            },
            render: function () {

                this.$el.html(CompanyTemplate);
                return this;

            }


        }
    );

    var CompanyListView = Backbone.View.extend({

        tagName:'tr',
        model:CompanyModel,
        events: {
            'click .editCompany': 'editCompany',
            'click .updateCompany': 'updateCompany',
            'click .cancelEdit': 'cancelEdit',
            'click .deleteCompany': 'deleteCompany'

        },

        editCompany:function () {

            this.$el.addClass("info");//D�zenlenecek sat?ra mavi renk veriliyor.
            this.$el.find("input").show().focus();//D�zenleme modunda imle� inputa getiriliyor.
            this.$el.find("span").hide();
            $(".editCompany").hide();
            $(".deleteCompany").hide();
            this.$el.find(".updateCompany").show();
            this.$el.find(".cancelEdit").show();

        },

        updateCompany:function () {
            var companyName= this.$el.find("#companyName").val();

            var company=new CompanyModel();
            company.set("companyName",companyName);
            this.$el.removeClass("info");
            if (!company.isValid()){

                $("." + newValues.validationError).tooltip("show");


            }else{

                this.$el.removeClass("info");
                this.model.set("companyName", companyName);


                this.model.save();

                $(".editCompany").show();
                $(".deleteCompany").show();
                this.render();


            }


        },
        cancelEdit:function () {
            this.$el.removeClass("info");
            $(".editCompany").show();
            $(".deleteCompany").show();
            this.render();

        },
        deleteCompany:function () {

            var that=this;


            if(uyar())
            {
                this.$el.find(".deleteCompany").addClass("disabled");
                var that = this;

                this.model.destroy({
                    wait: true,success: function () {
                        that.remove();

                    },

                }); // HTTP DELETE

            }else{

                return false;
            }





        },


        render: function () {
            var that = this;

            var template = Handlebars.compile(CompanyListTemplate);
            var myHtml = template(that.model.toJSON());

            that.$el.html(myHtml);

            return this;

        }



    });
    return {
        CompanyView:CompanyView,
        CompanyListView:CompanyListView

    }


});

