define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/UserModel',
    'text!app/templates/NewUserTemplate.html',
    'app/collections/ControlUsername',

], function($, _,Backbone,Handlebars,UserModel,NewUserTemplate,ControlUsername) {

    var NewUser = Backbone.View.extend({

            el: '.page',
        events:{
            'click #new':'new',


        },
        new:function () {


                var username=$("#username").val();
        var user=new UserModel();

            user.set("username",$("#username").val());
            user.set('firstLastName',$("#nameSurname").val());
            user.set('enable',0);
            user.set('password',$("#password").val());
            user.set('role',"tanimsiz");



            if(!user.isValid())
            {

                $("." + user.validationError).tooltip("show");
                return false;

            }else {
                debugger
                var control=new ControlUsername({
                    username:username,

                });

                control.fetch({

                    success:function (m_user) {

                        if(!m_user.size()==0)
                        {

                            $('#ornekModal').modal('show');

                            return false;

                        }

                        else {

                            debugger
                            user.save();
                            window.location.href='#/';
                            return false;




                        }

                    }

                });

                return false;
            }

            return false;
        },
            render: function () {

                this.$el.html(NewUserTemplate);
                return this;

            }


        }
    );
    return NewUser;


});

