define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/PlaceModel',
    'app/collections/PlaceCollection',
    'text!app/templates/SelectPlaceTemplate.html',


], function($, _,Backbone,Handlebars,PlaceModel,PlaceCollection,SelectPlaceTemplate){

    var placeCollection=new PlaceCollection();

    var SelectPlaceList=Backbone.View.extend({
        render:function (placeCollection) {

            var template = Handlebars.compile(SelectPlaceTemplate);
            var myHtml = template({places: placeCollection.toJSON()});
            $("#entryPlaceList").html(myHtml);



        }



    });


    return {
        placeCollection:placeCollection,
        SelectPlaceList:SelectPlaceList

    }




});


