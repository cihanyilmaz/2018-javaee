package com.toyota.resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.toyota.model.EntryCause;

import com.toyota.service.EntryCauseService;

import java.util.List;

/**
 * Created by Cihan Y?lmaz on 27.01.2018.
 */
@Component
@Path("/entryCause")
public class EntryCauseResource {

    @Autowired
    private EntryCauseService entryCauseService;

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public EntryCause save(EntryCause entryCause)
    {

        return entryCauseService.save(entryCause);
    }

    @GET
    @Produces("application/json")
    public List<EntryCause> all()
    {

        return entryCauseService.all();
    }

    @GET
    @Path("/{entryCauseId}")
    @Produces("application/json")
    @Consumes("application/json")
    public EntryCause findId(@PathParam("entryCauseId") int entryCauseId){

        return entryCauseService.findId(entryCauseId);
    }

    @PUT
    @Path("/{entryCauseId}")
    @Consumes("application/json")
    @Produces("application/json")
    public void edit(@PathParam("entryCauseId") int entryCauseId,EntryCause entryCause)
    {

        entryCauseService.edit(entryCause);



    }

    @GET
    @Path("/entryCauseName/{entryCauseName}")
    @Produces("application/json")
    @Consumes("application/json")
    public  List<EntryCause> findUsername(@PathParam("entryCauseName") String entryCauseName){

        return entryCauseService.controlEntryCause(entryCauseName);
    }

    @DELETE
    @Path("/{entryCauseId}")
    @Produces("text/plain")
    public int delete(@PathParam("entryCauseId") int entryCauseId)
    {   entryCauseService.delete(entryCauseId);

        return entryCauseId;

    }


}
