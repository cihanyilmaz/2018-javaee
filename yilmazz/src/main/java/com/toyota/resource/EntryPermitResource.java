package com.toyota.resource;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.toyota.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.toyota.model.EntryPermit;
import com.toyota.model.Place;

import com.toyota.service.EntryPermitService;
/**
 * Created by Cihan Y?lmaz on 29.01.2018.
 */
@Component
@Path("/entryPermit")
public class EntryPermitResource {

    @Autowired
    private EntryPermitService entryPermitService;

    @Autowired
    private PlaceService placeService;

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public EntryPermit save(EntryPermit entryPermit)
    {
        //Place place=placeService.findId(1);
       // place.getEntryPermitList().add(entryPermit);
        //entryPermit.getPlaceList().add(place);

        return entryPermitService.save(entryPermit);
    }

    @GET
    @Produces("application/json")
    public List<EntryPermit> all()
    {

        return entryPermitService.all();
    }

    @GET
    @Produces("application/json")
    @Path("/jobSecure")
    public List<EntryPermit> jobSecureAll(){

        return entryPermitService.jobSecureAll();

    }

    @GET
    @Path("/{entryPermitId}")
    @Produces("application/json")
    @Consumes("application/json")
    public EntryPermit findId(@PathParam("entryPermitId") int entryPermitId){

        return entryPermitService.findId(entryPermitId);
    }


    @PUT
    @Path("/{entryPermitId}")
    @Consumes("application/json")
    @Produces("application/json")
    public void edit(@PathParam("entryPermitId") int entryPermitId,EntryPermit entryPermit)
    {

        entryPermitService.edit(entryPermit);



    }


    @PUT
    @Path("/jobSecure/{entryPermitId}")
    @Consumes("application/json")
    @Produces("application/json")
    public void editJobSecure(@PathParam("entryPermitId") int entryPermitId,EntryPermit entryPermit)
    {

        entryPermitService.edit(entryPermit);



    }

    @GET
    @Path("allCompanys/{id}")
    @Produces("application/json")
    public List<EntryPermit> allCompany(@PathParam("id") int id)
    {

        return entryPermitService.allCompany(id);
    }

}
