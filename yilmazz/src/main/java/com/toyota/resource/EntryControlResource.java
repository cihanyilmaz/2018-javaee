package com.toyota.resource;

import com.toyota.model.EntryPermit;
import com.toyota.service.EntryPermitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.toyota.service.EntryControlService;

import com.toyota.model.EntryControl;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by Cihan Y?lmaz on 14.02.2018.
 */
@Component
@Path("/entryControl")
public class EntryControlResource {

    @Autowired
    private EntryControlService entryControlService;

    @Autowired
    private EntryPermitService entryPermitService;

    @GET
    @Path("allCompany/{id}")
    @Produces("application/json")
    public List<EntryPermit> all(@PathParam("id") int id)
    {

        return entryPermitService.allCompany(id);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public EntryControl save(EntryControl entryControl)
    {
        //Place place=placeService.findId(1);
        // place.getEntryPermitList().add(entryPermit);
        //entryPermit.getPlaceList().add(place);

        return entryControlService.save(entryControl);
    }

    @PUT
    @Path("/{entryControlId}")
    @Consumes("application/json")
    @Produces("application/json")
    public void editControl(@PathParam("entryControlId") int entryControlId,EntryControl entryControl)
    {

        entryControlService.edit(entryControl);



    }

    @GET
    @Path("/searchAdmin/{id}/{beginDate}/{endDate}")
    @Produces("application/json")
    public List<EntryControl> searchAdminTravel(@PathParam("id") int  id,@PathParam("beginDate") String  beginDate,@PathParam("endDate") String endDate){


        return entryControlService.search(id,beginDate,endDate);


    }



}
