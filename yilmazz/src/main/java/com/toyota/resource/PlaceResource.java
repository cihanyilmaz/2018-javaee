package com.toyota.resource;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.toyota.model.Place;
import com.toyota.service.PlaceService;
/**
 * Created by Cihan Y?lmaz on 27.01.2018.
 */

@Component
@Path("/place")
public class PlaceResource {

    @Autowired
    private PlaceService placeService;

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Place save(Place place)
    {

        return placeService.save(place);
    }

    @GET
    @Produces("application/json")
    public List<Place> all()
    {

        return placeService.all();
    }

    @GET
    @Path("/{placeId}")
    @Produces("application/json")
    @Consumes("application/json")
    public Place findId(@PathParam("placeId") int placeId){

        return placeService.findId(placeId);
    }

    @PUT
    @Path("/{placeId}")
    @Consumes("application/json")
    @Produces("application/json")
    public void edit(@PathParam("placeId") int placeId,Place place)
    {

        placeService.edit(place);



    }

    @GET
    @Path("/placeName/{placeName}")
    @Produces("application/json")
    @Consumes("application/json")
    public  List<Place> findUsername(@PathParam("placeName") String placeName){

        return placeService.controlPlace(placeName);
    }

    @DELETE
    @Path("/{placeId}")
    @Produces("text/plain")
    public int delete(@PathParam("placeId") int placeId)
    {   placeService.delete(placeId);

        return placeId;

    }


}
