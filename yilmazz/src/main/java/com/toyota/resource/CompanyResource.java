package com.toyota.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.toyota.model.Company;

import com.toyota.service.CompanyService;

@Component
@Path("/company")
public class CompanyResource {

    @Autowired
    private CompanyService companyService;

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Company save(Company company)
    {

        return companyService.save(company);
    }

    @GET
    @Produces("application/json")
    public List<Company> all()
    {

        return companyService.all();
    }

    @GET
    @Path("/{companyId}")
    @Produces("application/json")
    @Consumes("application/json")
    public Company findId(@PathParam("companyId") int companyId){

        return companyService.findId(companyId);
    }

    @PUT
    @Path("/{companyId}")
    @Consumes("application/json")
    @Produces("application/json")
    public void edit(@PathParam("companyId") int companyId,Company company)
    {

        companyService.edit(company);



    }

    @GET
    @Path("/companyName/{companyName}")
    @Produces("application/json")
    @Consumes("application/json")
    public  List<Company> findUsername(@PathParam("companyName") String companyName){

        return companyService.controlCompany(companyName);
    }

    @DELETE
    @Path("/{companyId}")
    @Produces("text/plain")
    public int delete(@PathParam("companyId") int companyId)
    {   companyService.delete(companyId);

        return companyId;

    }

}
