package com.toyota.service;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.toyota.dao.UserDao;
import com.toyota.model.User;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserDao userDao;


	public User save(User user)
	{

		try {
			return userDao.save(user);
		} catch (Exception e) {
			return null;
		}


	}
	public void edit(User user)
	{

		try {
			userDao.edit(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<User> controlUsername(String username){
		try {
			return userDao.controlUsername(username);
		} catch (Exception e) {
			return null;
		}


	}

	public User findId(int id){
		try {
			return userDao.findId(id);
		} catch (Exception e) {
			return null;
		}


	}

	public List<User> all()
	{
		try {
			return userDao.all();
		} catch (Exception e) {
			return null;
		}

		
	}
	public User login(String username){
		try {
			return userDao.login(username);
		} catch (Exception e) {
			return null;
		}



	}
}
