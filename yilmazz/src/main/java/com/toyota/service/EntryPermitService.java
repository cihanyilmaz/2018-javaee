package com.toyota.service;
import com.toyota.dao.EntryPermitDao;
import com.toyota.model.EntryPermit;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
/**
 * Created by Cihan Y?lmaz on 29.01.2018.
 */
@Service
@Transactional
public class EntryPermitService {

    @Autowired
    private EntryPermitDao entryPermitDao;

    public EntryPermit save(EntryPermit entryPermit)
    {
        try {
            return entryPermitDao.save(entryPermit);
        } catch (Exception e) {
            return null;
        }


    }

    public List<EntryPermit> all()
    {
        try {
            return entryPermitDao.all();
        } catch (Exception e) {
            return null;
        }


    }

    public List<EntryPermit> jobSecureAll(){
        try {
            return entryPermitDao.jobSecureAll();

        } catch (Exception e) {
            return null;
        }

    }

    public EntryPermit findId(int id){
        try {
            return entryPermitDao.findId(id);
        } catch (Exception e) {
            return null;
        }


    }

    public void edit(EntryPermit entryPermit)
    {

        try {
            entryPermitDao.edit(entryPermit);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public List<EntryPermit> allCompany(int id)
    {
        try {
            return entryPermitDao.allCompany(id);
        } catch (Exception e) {
            return null;
        }


    }
}
