package com.toyota.service;
import com.toyota.dao.EntryCauseDao;
import com.toyota.model.EntryCause;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
/**
 * Created by Cihan Y?lmaz on 27.01.2018.
 */
@Service
@Transactional
public class EntryCauseService {

    @Autowired
    private EntryCauseDao entryCauseDao;

    public EntryCause save(EntryCause entryCause)
    {
        try {
            return entryCauseDao.save(entryCause);
        } catch (Exception e) {
            return null;
        }


    }

    public List<EntryCause> controlEntryCause(String entryCause){

        try {
            return entryCauseDao.controlEntryCause(entryCause);
        } catch (Exception e) {
            return null;
        }


    }

    public List<EntryCause> all()
    {
        try {
            return entryCauseDao.all();
        } catch (Exception e) {
            return null;
        }


    }

    public void edit(EntryCause entryCause)
    {
        try {
            entryCauseDao.edit(entryCause);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public EntryCause findId(int id){

        try {
            return entryCauseDao.findId(id);
        } catch (Exception e) {
            return null;
        }

    }
    public void delete(int id)
    {
        try {
            entryCauseDao.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
