package com.toyota.service;

import com.toyota.model.EntryPermit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.toyota.dao.EntryControlDao;
import com.toyota.model.EntryControl;

import java.util.List;

/**
 * Created by Cihan Y?lmaz on 14.02.2018.
 */
@Service
@Transactional
public class EntryControlService {

    @Autowired
    private EntryControlDao entryControlDao;

    public EntryControl save(EntryControl entryControl)
    {
        try {
            return entryControlDao.save(entryControl);
        } catch (Exception e) {
            return null;
        }


    }

    public void edit(EntryControl entryControl)
    {

        try {
            entryControlDao.edit(entryControl);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public List<EntryControl> search(int id,String beginDate,String endDate)
    {
        try {
            return entryControlDao.search(id,beginDate,endDate);
        } catch (Exception e) {
            return null;
        }


    }

}
