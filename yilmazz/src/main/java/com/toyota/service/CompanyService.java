package com.toyota.service;

import com.toyota.dao.CompanyDao;
import com.toyota.model.Company;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Cihan Y?lmaz on 26.01.2018.
 */
@Service
@Transactional
public class CompanyService {

    @Autowired
    private CompanyDao companyDao;

    public Company save(Company company)
    {
        try {
            return companyDao.save(company);
        } catch (Exception e) {
            return null;
        }


    }

    public List<Company> controlCompany(String companyName){

        try {
            return companyDao.controlCompany(companyName);
        } catch (Exception e) {
            return null;
        }


    }

    public List<Company> all()
    {
        try {
            return companyDao.all();
        } catch (Exception e) {
            return null;
        }


    }

    public void edit(Company company)
    {
        try {
            companyDao.edit(company);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public Company findId(int id){

        try {
            return companyDao.findId(id);
        } catch (Exception e) {
            return null;
        }

    }
    public void delete(int id)
    {
        try {
             companyDao.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
