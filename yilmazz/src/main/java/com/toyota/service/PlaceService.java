package com.toyota.service;
import com.toyota.dao.PlaceDao;
import com.toyota.model.Place;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
/**
 * Created by Cihan Y?lmaz on 27.01.2018.
 */
@Service
@Transactional
public class PlaceService {

    @Autowired
    private PlaceDao placeDao;

    public Place save(Place company)
    {
        try {
            return placeDao.save(company);
        } catch (Exception e) {
            return null;
        }


    }

    public List<Place> controlPlace(String placeName){

        try {
            return placeDao.controlPlace(placeName);
        } catch (Exception e) {
            return null;
        }


    }

    public List<Place> all()
    {
        try {
            return placeDao.all();
        } catch (Exception e) {
            return null;
        }


    }

    public void edit(Place company)
    {
        try {
            placeDao.edit(company);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public Place findId(int id){

        try {
            return placeDao.findId(id);
        } catch (Exception e) {
            return null;
        }

    }
    public void delete(int id)
    {
        try {
            placeDao.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
