package com.toyota.dao;

import com.toyota.model.Place;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Cihan Y?lmaz on 27.01.2018.
 */
@Repository
public class PlaceDao {

    @PersistenceContext
    public EntityManager entityManager;


    public Place save(Place place)
    {
        try {
            //sessionFactory.getCurrentSession().save(user);
            entityManager.persist(place);
            return place;

        } catch (Exception e) {
            return null;
        }

    }

    public List<Place> controlPlace(String placeName){
        try {
            //Criteria criteria =sessionFactory.getCurrentSession().createCriteria(User.class);
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Place.class);
            criteria.add(Restrictions.eq("placeName", placeName));

            List results = criteria.list();
            return results;

        } catch (Exception e) {
            return null;
        }

    }

    public List<Place> all()
    {
        try {
            //Session session = this.sessionFactory.getCurrentSession();
            //List<User> musteriListe = session.createQuery("FROM User").list();
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Place.class);
            List results = criteria.list();
            return results;

        } catch (Exception e) {
            return null;
        }

    }


    public void edit(Place place)
    {
        try {
            //sessionFactory.getCurrentSession().update(user);
            entityManager.merge(place);

        } catch (Exception e) {

            e.printStackTrace();

        }



    }

    public Place findId(int id){
        try {
            return entityManager.find(Place.class, id);
            //sessionFactory.getCurrentSession().get(User.class, id);

        } catch (Exception e) {
            return null;
        }


    }
    public void delete(int id)
    {
        try {
            //sessionFactory.getCurrentSession().delete(findId(id));
            entityManager.remove(findId(id));

        } catch (Exception e) {

            e.printStackTrace();

        }



    }

}
