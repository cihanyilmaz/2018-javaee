package com.toyota.dao;
import com.toyota.model.EntryPermit;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
/**
 * Created by Cihan Y?lmaz on 29.01.2018.
 */
@Repository
public class EntryPermitDao {

    @PersistenceContext
    public EntityManager entityManager;

    public EntryPermit save(EntryPermit entryPermit)
    {
        try {
            //sessionFactory.getCurrentSession().save(user);
            entityManager.persist(entryPermit);
            return entryPermit;

        } catch (Exception e) {
            return null;
        }

    }


    public List<EntryPermit> all()
    {
        try {
            //Session session = this.sessionFactory.getCurrentSession();
            //List<User> musteriListe = session.createQuery("FROM User").list();
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(EntryPermit.class);
            criteria.add(Restrictions.eq("entryPermit", false));
            List results = criteria.list();
            return results;

        } catch (Exception e) {
            return null;
        }

    }

    public List<EntryPermit> jobSecureAll(){
        try {
            //Criteria criteria =sessionFactory.getCurrentSession().createCriteria(User.class);
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(EntryPermit.class);
            criteria.add(Restrictions.eq("jobSecure", true));

            List results = criteria.list();
            return results;

        } catch (Exception e) {
            return null;
        }

    }

    public EntryPermit findId(int id){
        try {
            return entityManager.find(EntryPermit.class, id);
            //sessionFactory.getCurrentSession().get(User.class, id);

        } catch (Exception e) {
            return null;
        }


    }

    public void edit(EntryPermit entryPermit)
    {
        try {
            //sessionFactory.getCurrentSession().update(user);
            entityManager.merge(entryPermit);

        } catch (Exception e) {

            e.printStackTrace();

        }



    }

    public List<EntryPermit> allCompany(int id)
    {
        try {
            //Session session = this.sessionFactory.getCurrentSession();
            //List<User> musteriListe = session.createQuery("FROM User").list();
           // Criteria criteria = entityManager.unwrap(Session.class).createCriteria(EntryPermit.class);
           // criteria.add(Restrictions.eq("company", id));
           // List results = criteria.list();
            Date s=new Date();
            Query query =entityManager.createNamedQuery("EntryPermit.findAll");
            query.setParameter(1, id);
            query.setParameter(2, s);
            List results=query.getResultList();
            return results;

        } catch (Exception e) {
           return  null;
        }

    }



}
