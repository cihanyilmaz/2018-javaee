package com.toyota.dao;
import com.toyota.model.EntryCause;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
/**
 * Created by Cihan Y?lmaz on 27.01.2018.
 */
@Repository
public class EntryCauseDao {

    @PersistenceContext
    public EntityManager entityManager;


    public EntryCause save(EntryCause entryCause)
    {
        try {
            //sessionFactory.getCurrentSession().save(user);
            entityManager.persist(entryCause);
            return entryCause;

        } catch (Exception e) {
            return null;
        }

    }

    public List<EntryCause> controlEntryCause(String entryCause){
        try {
            //Criteria criteria =sessionFactory.getCurrentSession().createCriteria(User.class);
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(EntryCause.class);
            criteria.add(Restrictions.eq("entryCauseName", entryCause));

            List results = criteria.list();
            return results;

        } catch (Exception e) {
            return null;
        }

    }

    public List<EntryCause> all()
    {
        try {
            //Session session = this.sessionFactory.getCurrentSession();
            //List<User> musteriListe = session.createQuery("FROM User").list();
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(EntryCause.class);
            List results = criteria.list();
            return results;

        } catch (Exception e) {
            return null;
        }

    }


    public void edit(EntryCause entryCause)
    {
        try {
            //sessionFactory.getCurrentSession().update(user);
            entityManager.merge(entryCause);

        } catch (Exception e) {

            e.printStackTrace();

        }



    }

    public EntryCause findId(int id){
        try {
            return entityManager.find(EntryCause.class, id);
            //sessionFactory.getCurrentSession().get(User.class, id);

        } catch (Exception e) {
            return null;
        }


    }
    public void delete(int id)
    {
        try {
            //sessionFactory.getCurrentSession().delete(findId(id));
            entityManager.remove(findId(id));

        } catch (Exception e) {

            e.printStackTrace();

        }



    }

}
