package com.toyota.dao;

import com.toyota.model.Company;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
/**
 * Created by Cihan Y?lmaz on 26.01.2018.
 */
@Repository
public class CompanyDao {

    @PersistenceContext
    public EntityManager entityManager;


    public Company save(Company company)
    {
        try {
            //sessionFactory.getCurrentSession().save(user);
            entityManager.persist(company);
            return company;

        } catch (Exception e) {
            return null;
        }

    }

    public List<Company> controlCompany(String companyName){
        try {
            //Criteria criteria =sessionFactory.getCurrentSession().createCriteria(User.class);
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Company.class);
            criteria.add(Restrictions.eq("companyName", companyName));

            List results = criteria.list();
            return results;

        } catch (Exception e) {
            return null;
        }

    }

    public List<Company> all()
    {
        try {
            //Session session = this.sessionFactory.getCurrentSession();
            //List<User> musteriListe = session.createQuery("FROM User").list();
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Company.class);
            List results = criteria.list();
            return results;

        } catch (Exception e) {
            return null;
        }

    }


    public void edit(Company company)
    {
        try {
            //sessionFactory.getCurrentSession().update(user);
            entityManager.merge(company);

        } catch (Exception e) {

            e.printStackTrace();

        }



    }

    public Company findId(int id){
        try {
            return entityManager.find(Company.class, id);
            //sessionFactory.getCurrentSession().get(User.class, id);

        } catch (Exception e) {
            return null;
        }


    }
    public void delete(int id)
    {
        try {
            //sessionFactory.getCurrentSession().delete(findId(id));
            entityManager.remove(findId(id));

        } catch (Exception e) {

            e.printStackTrace();

        }



    }



}
