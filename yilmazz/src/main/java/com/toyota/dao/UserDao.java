package com.toyota.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.toyota.model.User;

@Repository
public class UserDao {

	@PersistenceContext
	public EntityManager entityManager;

	public User save(User user)
	{
		try {
			//sessionFactory.getCurrentSession().save(user);
			entityManager.persist(user);
			return user;

		} catch (Exception e) {
			return null;
		}

	}

	public List<User> controlUsername(String username){
		try {
			//Criteria criteria =sessionFactory.getCurrentSession().createCriteria(User.class);
			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(User.class);
			criteria.add(Restrictions.eq("username", username));

			List results = criteria.list();
			return results;

		} catch (Exception e) {
			return null;
		}

	}

	public List<User> all()
	{
		try {
			//Session session = this.sessionFactory.getCurrentSession();
			//List<User> musteriListe = session.createQuery("FROM User").list();
			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(User.class);
			List results = criteria.list();
			return results;

		} catch (Exception e) {
			return null;
		}

	}

	public User login(String username){
		try {
			//Criteria criteria =sessionFactory.getCurrentSession().createCriteria(User.class);
			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(User.class);
			criteria.add(Restrictions.eq("username", username));
			return (User) criteria.uniqueResult();

		} catch (Exception e) {
			return null;
		}



	}
	public void edit(User user)
	{
		try {
			//sessionFactory.getCurrentSession().update(user);
			entityManager.merge(user);

		} catch (Exception e) {

			e.printStackTrace();

		}



	}

	public User findId(int id){
		try {
			return entityManager.find(User.class, id);
			//sessionFactory.getCurrentSession().get(User.class, id);

		} catch (Exception e) {
			return null;
		}


	}


}
