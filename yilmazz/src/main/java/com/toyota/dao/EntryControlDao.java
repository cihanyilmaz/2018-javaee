package com.toyota.dao;

import com.toyota.model.EntryPermit;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.toyota.model.EntryControl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Cihan Y?lmaz on 14.02.2018.
 */
@Repository
public class EntryControlDao {


    @PersistenceContext
    public EntityManager entityManager;


    public EntryControl save(EntryControl entryControl)
    {
        try {
            //sessionFactory.getCurrentSession().save(user);
            entityManager.persist(entryControl);
            return entryControl;

        } catch (Exception e) {
            return null;
        }

    }

    public void edit(EntryControl entryControl)
    {
        try {
            //sessionFactory.getCurrentSession().update(user);
            entityManager.merge(entryControl);

        } catch (Exception e) {

            e.printStackTrace();

        }



    }
    public List<EntryControl> search(int id,String beginDate,String endDate)
    {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date i = null,s = null;

            try {
                i = df.parse(beginDate);

                s =df.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            //Session session = this.sessionFactory.getCurrentSession();
            // Criteria criteria = session.createCriteria(Travel.class);
            Criteria criteria = entityManager.unwrap(Session.class).createCriteria(EntryControl.class);
            criteria.add(Restrictions.ge("company.id", id));
            criteria.add(Restrictions.ge("entryDate", i));
            criteria.add(Restrictions.lt("entryDate", s));
            List results = criteria.list();
            return results;

        } catch (Exception e) {
            return  null;
        }

    }


}
