package com.toyota.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Cihan Y?lmaz on 14.02.2018.
 */
@Entity
@NamedQuery(name = "EntryControl.search", query = "select e from EntryControl e where e.company.id = ?1 and  (e.entryDate between ?2 and ?3)")
public class EntryControl {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    String person;

    @Temporal(TemporalType.DATE)
    private Date entryDate;

    private String entryCompany;


    private String exitCompany;

    @ManyToOne
    private Company company;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public String getEntryCompany() {
        return entryCompany;
    }

    public void setEntryCompany(String entryCompany) {
        this.entryCompany = entryCompany;
    }

    public String getExitCompany() {
        return exitCompany;
    }

    public void setExitCompany(String exitCompany) {
        this.exitCompany = exitCompany;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
