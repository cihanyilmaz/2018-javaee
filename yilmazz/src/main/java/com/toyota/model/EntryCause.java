package com.toyota.model;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Cihan Y?lmaz on 27.01.2018.
 */
@Entity
public class EntryCause {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String entryCauseName;





    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEntryCauseName() {
        return entryCauseName;
    }

    public void setEntryCauseName(String entryCauseName) {
        this.entryCauseName = entryCauseName;
    }
}
