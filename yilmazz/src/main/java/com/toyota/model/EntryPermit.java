package com.toyota.model;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Cihan Y?lmaz on 28.01.2018.
 */
@Entity
@Table(name = "entrypermit")
@NamedQuery(name = "EntryPermit.findAll", query = "select e from EntryPermit e where e.company.id = ?1 and  e.entryDate=?2")
public class EntryPermit {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    String entryPersons;

    @Temporal(TemporalType.DATE)
    private Date entryDate;

    @Temporal(TemporalType.DATE)
    private Date exitDate;

   // @Temporal(TemporalType.TIMESTAMP)
    private String entryCompany;

    //@Temporal(TemporalType.TIMESTAMP)
    private String exitCompany;

    private String places;

    private String entryCauses;

    @ManyToOne
    private Company company;



    public String getPlaces() {
        return places;
    }

    public void setPlaces(String places) {
        this.places = places;
    }

    public String getEntryCauses() {
        return entryCauses;
    }

    public void setEntryCauses(String entryCauses) {
        this.entryCauses = entryCauses;
    }

    String withPerson;

    public String getEntryCompany() {
        return entryCompany;
    }

    public void setEntryCompany(String entryCompany) {
        this.entryCompany = entryCompany;
    }

    public String getExitCompany() {
        return exitCompany;
    }

    public void setExitCompany(String exitCompany) {
        this.exitCompany = exitCompany;
    }

    private boolean jobSecure;

    private boolean entryPermit;

    public boolean isEntryPermit() {
        return entryPermit;
    }

    public void setEntryPermit(boolean entryPermit) {
        this.entryPermit = entryPermit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEntryPersons() {
        return entryPersons;
    }

    public void setEntryPersons(String entryPersons) {
        this.entryPersons = entryPersons;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public Date getExitDate() {
        return exitDate;
    }

    public void setExitDate(Date exitDate) {
        this.exitDate = exitDate;
    }



    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }



    public String getWithPerson() {
        return withPerson;
    }

    public void setWithPerson(String withPerson) {
        this.withPerson = withPerson;
    }

    public boolean isJobSecure() {
        return jobSecure;
    }

    public void setJobSecure(boolean jobSecure) {
        this.jobSecure = jobSecure;
    }
}
