package com.toyota;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.stereotype.Service;

@Service
@Path("/prime")
public class Start {

	@GET
	@Path("/{number}")
	@Produces("text/plain")
	public boolean isTrue(@PathParam("number") int value)
	{
		return true;
	}
	
}
