define(['jquery', 'underscore', 'backbone', 'app/models/EntryControlModel'], function ($, _, Backbone, EntryControlModel) {


    var Search = Backbone.Collection.extend({

        model: EntryControlModel,
        initialize: function (options) {
            this.id = options.id;
            this.beginDate = options.beginDate;
            this.endDate=options.endDate;

        },
        url:function () {



            return "rest/entryControl/searchAdmin/" + this.id + "/"
                + this.beginDate + "/" + this.endDate;

        }




    });



    return Search;

});

