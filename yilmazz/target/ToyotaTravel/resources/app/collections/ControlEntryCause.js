define(['jquery', 'underscore', 'backbone', 'app/models/EntryCauseModel'], function ($, _, Backbone, EntryCauseModel) {


    var ControlEntryCause=Backbone.Collection.extend({
        model: EntryCauseModel,
        initialize: function ( options) {
            this.entryCauseName = options.entryCauseName;


        },
        url:function () {
            return "rest/entryCause/entryCauseName/"
                + this.entryCauseName;

        }


    });


    return ControlEntryCause;





});



