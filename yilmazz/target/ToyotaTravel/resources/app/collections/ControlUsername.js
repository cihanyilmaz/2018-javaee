define(['jquery', 'underscore', 'backbone', 'app/models/UserModel'], function ($, _, Backbone, UserModel) {


    var ControlUsername=Backbone.Collection.extend({
        model: UserModel,
        initialize: function ( options) {
            this.username = options.username;


        },
        url:function () {
            return "rest/user/username/"
                + this.username;

        }


    });


    return ControlUsername;





});
