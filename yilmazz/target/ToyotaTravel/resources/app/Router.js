define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'moment',
    'spin',
    'app/views/HomeView',
    'app/views/NewUserView',
    'app/views/PanelView',
    'app/views/AccountApprovalView',
    'app/collections/UserCollection',
    'app/views/CompanyView',
    'app/collections/CompanyCollection',
    'app/views/PlaceView',
    'app/collections/PlaceCollection',
    'app/views/EntryCauseView',
    'app/collections/EntryCauseCollection',
    'app/views/EntryPermitView',
    'app/views/SelectCompanyView',
    'app/views/SelectPlaceView',
    'app/views/SelectEntryCauseView',
    'app/views/EntryWaitView',
    'app/collections/EntryPermitCollection',
    'app/views/JobSecureView',
    'app/collections/JobSecureCollection',
    'app/views/EntryControlView',
    'app/views/ReportView',
    'app/models/LoginModel',


    


], function($, _,Backbone,Handlebars,moment,Spinner,HomeView,NewUserView,PanelView,AccountApprovalView,UserCollection,CompanyView,
            CompanyCollection,PlaceView,PlaceCollection,EntryCauseView,EntryCauseCollection,EntryPermitView,SelectCompanyView,SelectPlaceView,
            SelectEntryCauseView,EntryWaitView,EntryPermitCollection,JobSecureView,JobSecureCollection,EntryControlView,ReportView,LoginModel



   ){

    var Router = Backbone.Router.extend({

        routes: {
            '': 'home',
            'newUser':'newUser',
            'panel':'panel',
            'accountApproval':'accountApproval',
            'newCompany':'newCompany',
            'newPlace':'newPlace',
            'newEntryCause':'newEntryCause',
            'newEntryPermit':'newEntryPermit',
            'entryPermit':'entryPermit',
            'jobSecure':'jobSecure',
            'entryControl':'entryControl',
            'report':'report'

        },
        initialize: function () {

            this.homeView=new HomeView();
            this.newUserView=new NewUserView();
            this.panelView=new PanelView();
            this.accountApprovalView=new AccountApprovalView.AccountApprovalView();
            this.companyView=new CompanyView.CompanyView();
            this.placeView=new PlaceView.PlaceView();
            this.entryCauseView=new EntryCauseView.EntryCauseView();
            this.entryPermit=new EntryPermitView();
            this.entryWaitView=new EntryWaitView.EntryWaitView();
            this.jobSecureView=new JobSecureView.JobSecureView();
            this.entryControlView=new EntryControlView.EntryControlView();
            this.reportView=new ReportView.ReportView();
        },

        home:function () {

            var that=this;
           that.homeView.render();
        },

        newUser:function(){

            var that =this;
            that.newUserView.render();

        },
        panel:function () {

            var that=this;
            that.panelView.render();
        },
        accountApproval:function () {
            var that=this;
            var loginModel=new LoginModel();
            loginModel.fetch({

                success:function (m_loginModel) {
                    if(m_loginModel.toJSON().role=="siteYetkilisi" || m_loginModel.toJSON().role=="yonetici")
                    {

                        that.accountApprovalView.render();

                        var spinner =new Spinner();
                        $('body').after(spinner.spin().el);

                        var users=new UserCollection();

                        users.fetch({

                            success:function (m_users) {

                                spinner.stop();
                                m_users.each(function (m_travel) {

                                    var userEvent=new AccountApprovalView.AccountApprovalEventView();
                                    userEvent.model=m_travel;

                                    $("#userList").append(userEvent.render().el);


                                    if(m_travel.toJSON().enable==true)
                                        $("#subscribeNews").prop("checked", true);



                                })



                            }


                        });




                    }


                    else
                    {

                        that.accountApprovalView.render();
                        //window.location.href = '#/panel';
                        $('#warningText').text("Giris yetkiniz yok");
                        $('#ornekModal').modal('show');
                        return false;
                    }



                }


            });


        },

        newCompany:function () {

            var loginModel=new LoginModel();
            loginModel.fetch({

                success:function (m_loginModel) {
                    if(m_loginModel.toJSON().role=="siteYetkilisi" ||
                        m_loginModel.toJSON().role=="yonetici"|| m_loginModel.toJSON().role=="personel")
                    {

                        var that =this;
                        that.companyView.render();

                        var companys=new CompanyCollection();

                        companys.fetch({
                            success:function (m_companys) {
                                m_companys.each(function (m_company) {

                                    var companyEvent=new CompanyView.CompanyListView();
                                    companyEvent.model=m_company;

                                    $("#companyTemplateTable").append(companyEvent.render().el);


                                })




                            }


                        });





                    }


                    else
                    {

                        window.location.href = '#/panel';
                        $('#warningText').text("Giris yetkiniz yok");
                        $('#ornekModal').modal('show');
                    }



                }


            });




        },

        newPlace:function (){

        var loginModel=new LoginModel();
    loginModel.fetch({

        success:function (m_loginModel) {
            if(m_loginModel.toJSON().role=="siteYetkilisi" || m_loginModel.toJSON().role=="yonetici"|| m_loginModel.toJSON().role=="personel")
            {

                var that = this;
                that.placeView.render();


                var places = new PlaceCollection();

                places.fetch({
                    success: function (m_places) {
                        m_places.each(function (m_place) {

                            var placeEvent = new PlaceView.PlaceListView();
                            placeEvent.model = m_place;

                            $("#placeTemplateTable").append(placeEvent.render().el);


                        })


                    }

                })





            }


            else
            {

                window.location.href = '#/panel';
                $('#warningText').text("Giris yetkiniz yok");
                $('#ornekModal').modal('show');
            }



        }


    });


        },

        newEntryCause:function () {
            var loginModel=new LoginModel();
            loginModel.fetch({

                success:function (m_loginModel) {
                    if(m_loginModel.toJSON().role=="siteYetkilisi" || m_loginModel.toJSON().role=="yonetici"|| m_loginModel.toJSON().role=="personel")
                    {
                        var that = this;
                        that.entryCauseView.render();


                        var entryCauses = new EntryCauseCollection();

                        entryCauses.fetch({
                            success: function (m_entryCauses) {
                                entryCauses.each(function (m_entryCause) {

                                    var entryCauseEvent = new EntryCauseView.EntryCauseListView();
                                    entryCauseEvent.model = m_entryCause;

                                    $("#entryCauseTemplateTable").append(entryCauseEvent.render().el);


                                })


                            }

                        })





                    }


                    else
                    {

                        window.location.href = '#/panel';
                        $('#warningText').text("Giris yetkiniz yok");
                        $('#ornekModal').modal('show');
                    }



                }


            });



        },

        newEntryPermit:function () {
            var that=this;
            var loginModel=new LoginModel();
            loginModel.fetch({

                success:function (m_loginModel) {
                    if(true)
                    {


                        that.entryPermit.render();

                        var listCompanys=new SelectCompanyView.SelectCompanyList();
                        var listPlaces=new SelectPlaceView.SelectPlaceList();
                        var listEntryCauses=new SelectEntryCauseView.SelectEntryCauseList();

                        SelectCompanyView.companyCollection.fetch({
                            success:function (m_companys) {
                                listCompanys.render(m_companys);
                            }


                        });

                        SelectPlaceView.placeCollection.fetch({
                            success:function (m_places) {
                                listPlaces.render(m_places);
                            }


                        });

                        SelectEntryCauseView.entryCauseCollection.fetch({
                            success:function (m_entryCauses) {
                                listEntryCauses.render(m_entryCauses);
                            }


                        });




                    }


                    else
                    {

                        window.location.href = '#/panel';
                        $('#warningText').text("Giris yetkiniz yok");
                        $('#ornekModal').modal('show');
                    }



                }


            });



        },

        entryPermit:function () {
            var that=this;
            var loginModel=new LoginModel();
            loginModel.fetch({

                success:function (m_loginModel) {
                    if(true)
                    {


                        that.entryWaitView.render();

                        var entryPermitCollection=new EntryPermitCollection();
                        entryPermitCollection.fetch({
                            success: function (m_entryPermits) {
                                m_entryPermits.each(function (m_entryPermit) {

                                    var entryPermitEvent = new EntryWaitView.EntryWaitListView();
                                    entryPermitEvent.model = m_entryPermit;

                                    $("#entryPermitTemplateTable").append(entryPermitEvent.render().el);


                                })


                            }

                        })




                    }


                    else
                    {

                        window.location.href = '#/panel';
                        $('#warningText').text("Giris yetkiniz yok");
                        $('#ornekModal').modal('show');
                    }



                }


            });



        },

        jobSecure:function () {

            var loginModel=new LoginModel();
            loginModel.fetch({

                success:function (m_loginModel) {
                    if(m_loginModel.toJSON().role=="siteYetkilisi" || m_loginModel.toJSON().role=="insanKaynaklari")
                    {

                        var that=this;
                        that.jobSecureView.render();

                        var jobSecureCollection=new JobSecureCollection();
                        jobSecureCollection.fetch({
                            success: function (m_entryPermits) {
                                m_entryPermits.each(function (m_entryPermit) {

                                    var entryPermitEvent = new JobSecureView.JobSecureListView();
                                    entryPermitEvent.model = m_entryPermit;

                                    $("#jobSecureTemplateTable").append(entryPermitEvent.render().el);


                                })


                            }

                        })




                    }


                    else
                    {

                        window.location.href = '#/panel';
                        $('#warningText').text("Giris yetkiniz yok");
                        $('#ornekModal').modal('show');
                    }



                }


            });


        },

        entryControl:function () {
            var that=this;
            var loginModel=new LoginModel();
            loginModel.fetch({

                success:function (m_loginModel) {
                    if(true)
                    {



                        that.entryControlView.render();

                        var listCompanys=new SelectCompanyView.SelectCompanyList();
                        SelectCompanyView.companyCollection.fetch({
                            success:function (m_companys) {
                                listCompanys.render(m_companys);
                            }


                        });



                    }






                }


            });

        },


        report:function () {
            var loginModel=new LoginModel();
            loginModel.fetch({

                success:function (m_loginModel) {
                    if(m_loginModel.toJSON().role=="yonetici" ||m_loginModel.toJSON().role=="siteYetkilisi" || m_loginModel.toJSON().role=="insanKaynaklari")
                    {

                        var that=this;
                        that.reportView.render();

                        var listCompanys=new SelectCompanyView.SelectCompanyList();
                        SelectCompanyView.companyCollection.fetch({
                            success:function (m_companys) {
                                listCompanys.render(m_companys);
                            }


                        });




                    }


                    else
                    {

                        window.location.href = '#/panel';
                        $('#warningText').text("Giris yetkiniz yok");
                        $('#ornekModal').modal('show');
                    }



                }


            });


        }





    });

    return Router;

});