define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/PlaceModel',
    'text!app/templates/PlaceTemplate.html',
    'text!app/templates/PlaceListTemplate.html',
    'app/collections/ControlPlace'

], function($, _,Backbone,Handlebars,PlaceModel,PlaceTemplate,PlaceListTemplate,ControlPlace) {

    var PlaceView = Backbone.View.extend({

            el: '.page',
            events:{
                'click #savePlace':'savePlace',


            },
        savePlace:function () {

                var placeName=$("#placeName").val();

                var place =new PlaceModel();

            place.set("placeName",placeName);
                if(!place.isValid())
                {

                    $("." + place.validationError).tooltip("show");
                    return false;

                }else {

                    var control=new ControlPlace({
                        placeName:placeName,

                    });

                    control.fetch({

                        success:function (m_place) {
                            debugger
                            if(!m_place.size()==0)
                            {

                                $('#ornekModal').modal('show');

                                return false;

                            }

                            else {

                                debugger
                                place.save();
                               // window.location.href='#/newPlace';
                                //location.reload();
                                var placeListView =new PlaceListView();
                                placeListView.model=place;
                                $("#placeTemplateTable").append(placeListView.render().el);

                                $("#placeName").val("");
                                return false;




                            }

                        }

                    });

                    return false;
                }

                return false;


            },
            render: function () {

                this.$el.html(PlaceTemplate);
                return this;

            }


        }
    );

    var PlaceListView = Backbone.View.extend({

        tagName:'tr',
        model:PlaceModel,
        events: {
            'click .editPlace': 'editPlace',
            'click .updatePlace': 'updatePlace',
            'click .cancelEdit': 'cancelEdit',
            'click .deletePlace': 'deletePlace'

        },

        editPlace:function () {

            this.$el.addClass("info");//D�zenlenecek sat?ra mavi renk veriliyor.
            this.$el.find("input").show().focus();//D�zenleme modunda imle� inputa getiriliyor.
            this.$el.find("span").hide();
            $(".editPlace").hide();
            $(".deletePlace").hide();
            this.$el.find(".updatePlace").show();
            this.$el.find(".cancelEdit").show();

        },

        updatePlace:function () {
            var placeName= this.$el.find("#placeName").val();

            var place=new PlaceModel();
            place.set("placeName",placeName);
            this.$el.removeClass("info");
            if (!place.isValid()){

                $("." + place.validationError).tooltip("show");


            }else{

                this.$el.removeClass("info");
                this.model.set("placeName", placeName);


                this.model.save();

                $(".editPlace").show();
                $(".deletePlace").show();
                this.render();


            }


        },
        cancelEdit:function () {
            this.$el.removeClass("info");
            $(".editPlace").show();
            $(".deletePlace").show();
            this.render();

        },
        deletePlace:function () {

            var that=this;


            if(uyar())
            {
                this.$el.find(".deletePlace").addClass("disabled");
                var that = this;

                this.model.destroy({
                    wait: true,success: function () {
                        that.remove();

                    },

                }); // HTTP DELETE

            }else{

                return false;
            }





        },


        render: function () {
            var that = this;

            var template = Handlebars.compile(PlaceListTemplate);
            var myHtml = template(that.model.toJSON());

            that.$el.html(myHtml);

            return this;

        }



    });
    return {
        PlaceView:PlaceView,
        PlaceListView:PlaceListView

    }


});


