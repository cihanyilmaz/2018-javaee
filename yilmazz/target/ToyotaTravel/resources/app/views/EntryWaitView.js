define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/EntryPermitModel',
    'text!app/templates/EntryPermitTemplate.html',
    'text!app/templates/EntryPermitListTemplate.html',


], function($, _,Backbone,Handlebars,EntryPermitModel,EntryPermitTemplate,EntryPermitListTemplate) {

    var EntryWaitView = Backbone.View.extend({

            el: '.page',

            render: function () {

                this.$el.html(EntryPermitTemplate);
                return this;

            }


        }
    );

    var EntryWaitListView = Backbone.View.extend({

        tagName:'tr',
        model:EntryPermitModel,
        events: {
            'click .entryWaitPermit': 'editCompanyEntryWaitPermit',


        },

        editCompanyEntryWaitPermit:function () {


            this.model.set("entryPermit", true);


            this.model.save();

            window.location.href='#/entryPermit';
            location.reload();

        },


        render: function () {
            var that = this;

            var template = Handlebars.compile(EntryPermitListTemplate);
            var myHtml = template(that.model.toJSON());

            that.$el.html(myHtml);

            return this;

        }



    });
    return {
        EntryWaitView:EntryWaitView,
        EntryWaitListView:EntryWaitListView

    }


});


