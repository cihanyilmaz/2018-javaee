define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/HomeTemplate.html'

], function($, _,Backbone,Handlebars,HomeTemplate) {

    var HomeView = Backbone.View.extend({

            el: '.page',
        events:{
            'click #newUserGo':'go',


        },
        go:function () {
            window.location.href='#/newUser';
            return false;
        },
            render: function () {

                this.$el.html(HomeTemplate);
                return this;

            }


        }
    );
    return HomeView;


});
