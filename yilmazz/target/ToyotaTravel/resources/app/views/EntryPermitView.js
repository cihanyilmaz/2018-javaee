define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/EntryPermitModel',
    'app/models/CompanyModel',
    'app/models/PlaceModel',
    'text!app/templates/NewEntryPermitTemplate.html',


], function($, _,Backbone,Handlebars,EntryPermitModel,CompanyModel,PlaceModel,NewEntryPermitTemplate) {

    var NewEntryPermit = Backbone.View.extend({

            el: '.page',
            events:{
                'click #saveEntryPermit':'saveEntryPermit',
                'click #notsaveEntryPermit':'notsaveEntryPermit',



            },
        saveEntryPermit:function () {
            var j=$("#yes").val();
            if($("#yes").is(':checked'))
                var jobSecure=true;
            else if($("#no").is(':checked'))
                var jobSecure=false;
            else
               var jobSecure=null;


            var places=$("#entryPlaceList").val();
            var entryCauses=$("#entryCauseList").val();
            var entryPersons=$('#entryPersons').val();
            var company=$("#companyList").val();
            var entryCompany=$("#entryCompany").val();
            var exitCompany=$("#exitCompany").val();
            var exitDate=$("#exitDate").val();
            var entryDate=$("#entryDate").val();
            var withPerson=$("#withPerson").val();


            var lastDate=new Date(exitDate);
            var firstDate=new Date(entryDate);

           var fark= Math.round((lastDate- firstDate) / (1000 * 60 * 60 * 24));

                //var fark_salise = Math.abs(exitDate - entryDate);
             //  var sonuc= Math.round(fark_salise/86400000);

           // var bakalim=Math.round((t- a) / (1000 * 60 * 60 * 24));

            var company=new CompanyModel({id:company});


            var index;
            var placeS="";
            for (index = 0; index < places.length; ++index) {
                console.log(places[index]);
                placeS=placeS+places[index];
            }

            var index2;
            var entryCauseS="";
            for (index2 = 0; index2 < entryCauses.length; ++index2) {
                console.log(entryCauses[index2]);
                entryCauseS=entryCauseS+entryCauses[index2]+" ";
            }

            debugger
            var oneEntryPerson=entryPersons.split("\n");

            oneEntryPerson.forEach(function (person) {

                console.log("came person:"+person);
                var entryPermit =new EntryPermitModel();


                entryPermit.set("company",company);
                entryPermit.set("places",placeS);
                entryPermit.set("entryCauses",entryCauseS);
                entryPermit.set("entryPermit",false);
                entryPermit.set("entryPersons",person);
                entryPermit.set("exitCompany",exitCompany);
                entryPermit.set("entryDate",entryDate);
                entryPermit.set("exitDate",exitDate);
                entryPermit.set("entryCompany",entryCompany);
                entryPermit.set("withPerson",withPerson);
                entryPermit.set("jobSecure",jobSecure);

            if(!entryPermit.isValid())
            {

                $("." + entryPermit.validationError).tooltip("show");
                return false;

            }else {

                if(fark>6)
                {

                    $("#entryDate").attr("title","gun farki 7 den fazla olamaz");
                    $("#entryDate").tooltip("show");
                    $("#entryDate").attr("title", "Bo? Gešilemez!!");
                    return false;
                }

                entryPermit.save();

                window.location.href='#/newEntryPermit';
                location.reload();
                return false;

            }




                return false;



            })



           // entryPermit.set("placeList",placeModel);

           //var placeMoel=new PlaceModel({id:"1"});
           // entryPermit.set("placeList",placeMoel);
            return false;


            },
        notsaveEntryPermit:function () {

            window.location.href='#/newEntryPermit';
            location.reload();
            return false;

        },
            render: function () {

                this.$el.html(NewEntryPermitTemplate);
                return this;

            }


        }
    );
    return NewEntryPermit;


});


