define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/UserModel',
    'text!app/templates/AccountApprovalTemplate.html',
    'text!app/templates/AccountApprovalListTemplate.html'

], function($, _,Backbone,Handlebars,UserModel,AccountApprovalTemplate,AccountApprovalListTemplate) {

    var AccountApprovalEventView=Backbone.View.extend({
        tagName:'tr',
        model:UserModel,
        events: {
            'click .saveUser': 'saveUser',

        },
        saveUser:function () {
debugger

            var user =new UserModel({id:$("#id").val()});

            var enable;
            var firsLastName=$("#againfirstLastName").text();
            var username=$("#againusername").text();
           var s= $(".subscribeNews").val()
            if( $(".subscribeNews").val()=="true" )
            {
                enable=1;
            }

            else
            {
                enable=0;
            }


            var role=$("#againroleApproval").val();
            user.fetch({

                success:function (m_user) {


                    m_user.set('firstLastName',firsLastName);
                    m_user.set('username',username);
                    m_user.set('enable',enable);
                    m_user.set('role',role);
                    m_user.save();
                    window.location.href='#/accountApproval';
                    location.reload();
                    return false;




                }


            });






        },

        render: function () {
            var that = this;

            var template = Handlebars.compile(AccountApprovalListTemplate);
            var myHtml = template(that.model.toJSON());

            that.$el.html(myHtml);

            return this;

        }




    });
    var AccountApprovalView = Backbone.View.extend({

            el: '.page',

            render: function () {

                this.$el.html(AccountApprovalTemplate);
                return this;

            }


        }
    );
    return {
        AccountApprovalEventView: AccountApprovalEventView,
        AccountApprovalView: AccountApprovalView
    };


});

