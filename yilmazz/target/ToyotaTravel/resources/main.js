require.config({

    baseUrl: "resources",
    waitSeconds: 100,
    paths: {
        'jquery': 'lib/jquery-3.3.1',
        'bootstrap': 'lib/bootstrap',
        'html5shiv': 'lib/html5shiv',
        'spin': 'lib/spin',
        'respond': 'lib/respond',
        'underscore': 'lib/underscore',
        'backbone': 'lib/backbone',
        'backbone-relational': 'lib/backbone-relational',
        'handlebars': 'lib/handlebars-v3.0.3',
        'template': 'app/templates',
        'moment':'lib/moment',
        'ek':'lib/ek',

    },
    shim: {
        html5shiv: {
            deps: ['jquery']
        },
        respond: {
            deps: ['jquery']
        },
        bootstrap: {
            deps: ['jquery']
        },
        jquery: {
            exports: '$'
        },
        underscore: {
            exports: '_'
        },

        backbone: {
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        },


        handlebars: {
            exports: "Handlebars"
        },
        ek:{

            deps: ['jquery', 'handlebars']
        }
    },
    urlArgs: "bust=" + (new Date()).getTime()
});

require([
    'jquery',
    'bootstrap',
    'html5shiv',
    'respond',
    'ek',
    'spin',
    'underscore',
    'backbone',

    'handlebars',
    'moment',
    'app/Router',

], function ($,
             $,
             $,
             $,
             $,
             Spinner,
             _,
             Backbone,

             Handlebars,
             moment,

             Router) {
    var router = new Router();
    Backbone.history.start();
});
